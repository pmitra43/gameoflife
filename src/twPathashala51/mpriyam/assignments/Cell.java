package twPathashala51.mpriyam.assignments;

//represents the coordinates and state of a single cell

import java.util.ArrayList;

public class Cell {
  private int x;
  private int y;

  private ArrayList<Cell> listOfNeighbours;

  private Cell(int x, int y) {
    this.x = x;
    this.y = y;
    listOfNeighbours = new ArrayList<>();
  }

  public static Cell create(int x, int y) {
    return new Cell(x, y);
  }

  private void createNeighbours() {
    int i, j;
    Cell anotherCell;
    for (i = this.x - 1; i <= this.x + 1; i++) {
      for (j = this.y - 1; j <= this.y + 1; j++) {
        if (i == x && j == y) {
          continue;
        }
        anotherCell = Cell.create(i, j);
        listOfNeighbours.add(anotherCell);
      }
    }
  }

  public ArrayList<Cell> getListOfNeighbours() {
    createNeighbours();
    return listOfNeighbours;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Cell cell = (Cell) o;

    if (x != cell.x) return false;
    return y == cell.y;

  }

  public void printCellCoordinates(){
    System.out.println(x + " " + y);
  }

  @Override
  public int hashCode() {
    int result = x;
    result = 31 * result + y;
    return result;
  }
}
