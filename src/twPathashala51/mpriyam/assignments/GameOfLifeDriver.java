package twPathashala51.mpriyam.assignments;

import java.util.*;

//the driver class for the Game of life, to take inputs and provide output from command line

public class GameOfLifeDriver {

  public static void main(String args[]) {
    int xCoOrdinate, yCoOrdinate, countOfLiveCells;
    Set<Cell> liveCells = new HashSet<>();

    Scanner sc = new Scanner(System.in);
    System.out.println("Provide the number of live cells in the current state --> ");
    countOfLiveCells = Integer.parseInt(sc.nextLine());

    for (int i = 0; i < countOfLiveCells; i++) {
      String inputString = sc.nextLine();
      String[] inputItems = inputString.split(",");
      xCoOrdinate = Integer.parseInt(inputItems[0]);
      yCoOrdinate = Integer.parseInt(inputItems[1]);
      liveCells.add(Cell.create(xCoOrdinate, yCoOrdinate));
    }
    System.out.println("Output: ");
    Set<Cell> nextLiveCells = new GameOfLife().getNextGeneration(liveCells);
    nextLiveCells.forEach(Cell::printCellCoordinates);
  }
}
