package twPathashala51.mpriyam.assignments;

import java.util.*;

//represents the game, with all the cells keeping a count of their live neighbours

public class GameOfLife {
  private HashMap<Cell, Integer> liveCellNeighbours;
  private Set<Cell> resultLiveCells;
  private HashMap<Cell, Integer> deadCellNeighbours;

  GameOfLife() {
    liveCellNeighbours = new HashMap<>();
    deadCellNeighbours = new HashMap<>();
    resultLiveCells = new HashSet<>();
  }

  public Set<Cell> getNextGeneration(Set<Cell> liveCells) {
    liveCells.forEach(this::createLiveCell);
    countExistingAliveCellsForNextTick();
    countNewReincarnatedDeadCellsForNextTick();
    return resultLiveCells;
  }

  private void createLiveCell(Cell cell) {
    if (liveCellNeighbours.containsKey(cell)) {
      return; //live cell already exists
    }
    if (deadCellNeighbours.containsKey(cell)) {
      liveCellNeighbours.put(cell, deadCellNeighbours.get(cell));
      deadCellNeighbours.remove(cell);
    } else {
      liveCellNeighbours.put(cell, 0);
    }
    updateNeighbours(cell);
  }

  private void updateNeighbours(Cell cell) {
    ArrayList<Cell> neighbourCells = cell.getListOfNeighbours();
    neighbourCells.forEach(this::addNeighbourCount);
  }

  private void addNeighbourCount(Cell anotherCell) {
    if (liveCellNeighbours.containsKey(anotherCell)) {
      liveCellNeighbours.put(anotherCell, liveCellNeighbours.get(anotherCell) + 1);
      return;
    }
    if (deadCellNeighbours.containsKey(anotherCell)) {
      deadCellNeighbours.put(anotherCell, deadCellNeighbours.get(anotherCell) + 1);
      return;
    }
    deadCellNeighbours.put(anotherCell, 1);

  }

  private void countExistingAliveCellsForNextTick() {
    Set entrySet = liveCellNeighbours.entrySet();
    Iterator it = entrySet.iterator();
    while (it.hasNext()) {
      Map.Entry me = (Map.Entry) it.next();
      if ((Integer) me.getValue() >= 2 && (Integer) me.getValue() <= 3)
        resultLiveCells.add((Cell) me.getKey());
    }
  }

  private void countNewReincarnatedDeadCellsForNextTick() {
    Set entrySet = deadCellNeighbours.entrySet();
    Iterator it = entrySet.iterator();
    while (it.hasNext()) {
      Map.Entry me = (Map.Entry) it.next();
      if ((Integer) me.getValue() == 3)
        resultLiveCells.add((Cell) me.getKey());
    }
  }
}
