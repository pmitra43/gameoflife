package twPathashala51.mpriyam.assignments;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class GameOfLifeTest {

  @Test
  /*
  XX
  XX
   */
  public void cellsWithTwoOrThreeNeighboursShouldLive() {
    Set<Cell> liveCells = new HashSet<>();
    Set<Cell> expectedCells = new HashSet<>();
    Cell cell = null;
    cell = Cell.create(0, 0);
    liveCells.add(cell);
    expectedCells.add(cell);
    cell = Cell.create(0, 1);
    liveCells.add(cell);
    expectedCells.add(cell);
    cell = Cell.create(1, 0);
    liveCells.add(cell);
    expectedCells.add(cell);
    cell = Cell.create(1, 1);
    liveCells.add(cell);
    expectedCells.add(cell);

    Assert.assertEquals(expectedCells, new GameOfLife().getNextGeneration(liveCells));
  }

  @Test
  /*
  X
   */
  public void cellsWithOneOrLessNeighboursShouldDie() {
    Set<Cell> liveCells = new HashSet<>();
    Set<Cell> expectedCells = new HashSet<>();
    Cell cell = null;
    cell = Cell.create(0, 0);
    liveCells.add(cell);
    Assert.assertEquals(expectedCells, new GameOfLife().getNextGeneration(liveCells));
  }

  @Test
  /*
  XX
  X-
   */
  public void deadCellsWithExactlyThreeNeighboursShouldComeToLife() {
    Set<Cell> liveCells = new HashSet<>();
    Set<Cell> expectedCells = new HashSet<>();
    Cell cell = null;
    cell = Cell.create(0, 0);
    liveCells.add(cell);
    expectedCells.add(cell);
    cell = Cell.create(0, 1);
    liveCells.add(cell);
    expectedCells.add(cell);
    cell = Cell.create(1, 0);
    liveCells.add(cell);
    expectedCells.add(cell);
    cell = Cell.create(1, 1);
    expectedCells.add(cell);

    Assert.assertEquals(expectedCells, new GameOfLife().getNextGeneration(liveCells));
  }

  @Test
  /*
  X-X
  XXX
   */
  public void liveCellsWithMoreThanThreeNeighboursShouldDie() {
    Set<Cell> liveCells = new HashSet<>();
    Set<Cell> expectedCells = new HashSet<>();
    Cell cell = null;
    cell = Cell.create(0, 0);
    liveCells.add(cell);
    expectedCells.add(cell);
    cell = Cell.create(0, 2);
    liveCells.add(cell);
    expectedCells.add(cell);
    cell = Cell.create(1, 0);
    liveCells.add(cell);
    expectedCells.add(cell);
    cell = Cell.create(1, 1);
    liveCells.add(cell);
    cell = Cell.create(1, 2);
    liveCells.add(cell);
    expectedCells.add(cell);
    cell = Cell.create(2, 1);
    expectedCells.add(cell);
    Assert.assertEquals(expectedCells, new GameOfLife().getNextGeneration(liveCells));
  }

}
