package twPathashala51.mpriyam.assignments;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class CellTest {

  @Test
  public void cellsWithSameCoOrdinatesShouldBeEqual() {
    Cell cell = Cell.create(1, 1);
    assertEquals(Cell.create(1, 1), cell);
  }

  @Test
  public void testListOfNeighbours() {
    Cell cell = Cell.create(1, 1);
    Cell anotherCell = null;
    ArrayList<Cell> neighbours = new ArrayList<>();
    anotherCell = Cell.create(0, 0);
    neighbours.add(anotherCell);
    anotherCell = Cell.create(0, 1);
    neighbours.add(anotherCell);
    anotherCell = Cell.create(0, 2);
    neighbours.add(anotherCell);
    anotherCell = Cell.create(1, 0);
    neighbours.add(anotherCell);
    anotherCell = Cell.create(1, 2);
    neighbours.add(anotherCell);
    anotherCell = Cell.create(2, 0);
    neighbours.add(anotherCell);
    anotherCell = Cell.create(2, 1);
    neighbours.add(anotherCell);
    anotherCell = Cell.create(2, 2);
    neighbours.add(anotherCell);
    assertEquals(neighbours, cell.getListOfNeighbours());
  }
}
